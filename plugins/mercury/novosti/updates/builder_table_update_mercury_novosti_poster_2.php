<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryNovostiPoster2 extends Migration
{
    public function up()
    {
        Schema::table('mercury_novosti_poster', function($table)
        {
            $table->text('partys')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('mercury_novosti_poster', function($table)
        {
            $table->dropColumn('partys');
        });
    }
}
