<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryNovostiSubscribe extends Migration
{
    public function up()
    {
        Schema::create('mercury_novosti_subscribe', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('e_mail');
            $table->string('subscriber_name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_novosti_subscribe');
    }
}
