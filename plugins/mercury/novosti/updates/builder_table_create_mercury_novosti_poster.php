<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryNovostiPoster extends Migration
{
    public function up()
    {
        Schema::create('mercury_novosti_poster', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->date('main_date');
            $table->string('title');
            $table->dateTime('period');
            $table->text('description');
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_novosti_poster');
    }
}
