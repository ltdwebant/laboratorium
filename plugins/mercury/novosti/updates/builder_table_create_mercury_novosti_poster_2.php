<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryNovostiPoster2 extends Migration
{
    public function up()
    {
        Schema::create('mercury_novosti_poster', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->text('partys');
            $table->date('date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_novosti_poster');
    }
}
