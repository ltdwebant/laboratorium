<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryNovostiNews extends Migration
{
    public function up()
    {
        Schema::create('mercury_novosti_news', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->text('text');
            $table->date('date');
            $table->string('appearence');
            $table->date('date_to_pub');
            $table->boolean('timer');
            $table->string('url_img');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_novosti_news');
    }
}
