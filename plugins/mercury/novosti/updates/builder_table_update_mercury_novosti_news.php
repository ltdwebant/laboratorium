<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryNovostiNews extends Migration
{
    public function up()
    {
        Schema::table('mercury_novosti_news', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('mercury_novosti_news', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
