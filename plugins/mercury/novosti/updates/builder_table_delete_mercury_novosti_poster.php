<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteMercuryNovostiPoster extends Migration
{
    public function up()
    {
        Schema::dropIfExists('mercury_novosti_poster');
    }
    
    public function down()
    {
        Schema::create('mercury_novosti_poster', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id')->unsigned();
            $table->date('main_date');
            $table->text('partys')->nullable();
            $table->primary(['id']);
        });
    }
}
