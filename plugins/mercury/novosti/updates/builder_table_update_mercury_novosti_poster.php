<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryNovostiPoster extends Migration
{
    public function up()
    {
        Schema::table('mercury_novosti_poster', function($table)
        {
            $table->time('party_time_end');
            $table->renameColumn('period', 'party_time_start');
        });
    }
    
    public function down()
    {
        Schema::table('mercury_novosti_poster', function($table)
        {
            $table->dropColumn('party_time_end');
            $table->renameColumn('party_time_start', 'period');
        });
    }
}
