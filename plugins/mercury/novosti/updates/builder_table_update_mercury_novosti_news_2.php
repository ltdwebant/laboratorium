<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryNovostiNews2 extends Migration
{
    public function up()
    {
        Schema::table('mercury_novosti_news', function($table)
        {
            $table->string('title', 255)->nullable()->change();
            $table->string('description', 255)->nullable()->change();
            $table->text('text')->nullable()->change();
            $table->date('date')->nullable()->change();
            $table->string('appearence', 255)->nullable()->change();
            $table->date('date_to_pub')->nullable()->change();
            $table->boolean('timer')->nullable()->change();
            $table->string('url_img', 255)->nullable()->change();
            $table->string('slug', 255)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('mercury_novosti_news', function($table)
        {
            $table->string('title', 255)->nullable(false)->change();
            $table->string('description', 255)->nullable(false)->change();
            $table->text('text')->nullable(false)->change();
            $table->date('date')->nullable(false)->change();
            $table->string('appearence', 255)->nullable(false)->change();
            $table->date('date_to_pub')->nullable(false)->change();
            $table->boolean('timer')->nullable(false)->change();
            $table->string('url_img', 255)->nullable(false)->change();
            $table->string('slug', 255)->nullable(false)->change();
        });
    }
}
