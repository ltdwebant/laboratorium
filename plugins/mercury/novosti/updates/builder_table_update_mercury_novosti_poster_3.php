<?php namespace Mercury\Novosti\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryNovostiPoster3 extends Migration
{
    public function up()
    {
        Schema::table('mercury_novosti_poster', function($table)
        {
            $table->dropColumn('title');
            $table->dropColumn('party_time_start');
            $table->dropColumn('description');
            $table->dropColumn('party_time_end');
        });
    }
    
    public function down()
    {
        Schema::table('mercury_novosti_poster', function($table)
        {
            $table->string('title', 255);
            $table->dateTime('party_time_start');
            $table->text('description');
            $table->time('party_time_end');
        });
    }
}
