<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryEducationCoursesTeachers extends Migration
{
    public function up()
    {
        Schema::create('mercury_education_courses_teachers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('course_id');
            $table->integer('teacher_id');
            $table->primary(['course_id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_education_courses_teachers');
    }
}
