<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteMercuryEducationDbteachersCourses extends Migration
{
    public function up()
    {
        Schema::dropIfExists('mercury_education_dbteachers_courses');
    }
    
    public function down()
    {
        Schema::create('mercury_education_dbteachers_courses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('teacher_id');
            $table->integer('course_id');
        });
    }
}
