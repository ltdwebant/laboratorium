<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryEducationDbteachers extends Migration
{
    public function up()
    {
        Schema::create('mercury_education_dbteachers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('slug')->nullable();
            $table->string('name')->nullable();
            $table->string('actvity')->nullable();
            $table->text('biography')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_education_dbteachers');
    }
}
