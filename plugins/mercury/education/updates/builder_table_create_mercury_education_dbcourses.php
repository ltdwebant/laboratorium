<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateMercuryEducationDbcourses extends Migration
{
    public function up()
    {
        Schema::create('mercury_education_dbcourses', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('course_title')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('mercury_education_dbcourses');
    }
}
