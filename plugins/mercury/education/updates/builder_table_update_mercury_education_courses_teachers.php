<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryEducationCoursesTeachers extends Migration
{
    public function up()
    {
        Schema::table('mercury_education_courses_teachers', function($table)
        {
            $table->dropPrimary(['course_id']);
            $table->integer('courses_id');
            $table->integer('teachers_id');
            $table->dropColumn('course_id');
            $table->dropColumn('teacher_id');
            $table->primary(['courses_id']);
        });
    }
    
    public function down()
    {
        Schema::table('mercury_education_courses_teachers', function($table)
        {
            $table->dropPrimary(['courses_id']);
            $table->dropColumn('courses_id');
            $table->dropColumn('teachers_id');
            $table->integer('course_id');
            $table->integer('teacher_id');
            $table->primary(['course_id']);
        });
    }
}
