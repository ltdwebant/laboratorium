<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryEducationCoursesTeachers2 extends Migration
{
    public function up()
    {
        Schema::table('mercury_education_courses_teachers', function($table)
        {
            $table->dropPrimary(['courses_id']);
        });
    }
    
    public function down()
    {
        Schema::table('mercury_education_courses_teachers', function($table)
        {
            $table->primary(['courses_id']);
        });
    }
}
