<?php namespace Mercury\Education\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateMercuryEducationDbteachers2 extends Migration
{
    public function up()
    {
        Schema::table('mercury_education_dbteachers', function($table)
        {
            $table->renameColumn('activity', 'actvity');
        });
    }
    
    public function down()
    {
        Schema::table('mercury_education_dbteachers', function($table)
        {
            $table->renameColumn('actvity', 'activity');
        });
    }
}
