<?php namespace Mercury\Education\Models;

use Model;

/**
 * Model
 */
class Courses extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'mercury_education_dbcourses';

    public $belongsToMany = [
        'teachers' =>[
            'mercury\Education\Models\teachers',
            'table' => 'mercury_education_courses_teachers',
            'order' => 'name'
        ]
    ];
}
