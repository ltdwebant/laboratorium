<?php return [
    'plugin' => [
        'name' => 'Образование',
        'description' => '',
    ],
    'education' => [
        'teacher_name' => 'Имя',
        'teacher_slug' => 'Ссылка',
        'teacher_activity' => 'Деятельность',
        'teacher_biography' => 'Биография',
        'manage_teachers' => 'Управление учителями',
        'teachers' => 'Учителя',
        'courses' => 'Курсы',
        'course_title' => 'Заголовок курса',
        'course_description' => 'Описание курса',
    ],
];