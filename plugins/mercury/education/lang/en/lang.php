<?php return [
    'plugin' => [
        'name' => 'Education',
        'description' => '',
    ],
    'education' => [
        'teacher_name' => 'Name',
        'teacher_slug' => 'Slug',
        'teacher_activity' => 'Activity',
        'teacher_biography' => 'Biography',
        'manage_teachers' => 'Manage Teachers',
        'teachers' => 'Teachers',
        'courses' => 'Courses',
        'course_title' => 'Course title',
        'course_description' => 'Course description',
    ],
];